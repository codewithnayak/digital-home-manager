using System;
using System.Linq;
using DigitalHome.Db;
using DigitalHome.Db.Ext;

namespace DigitalHomeManager.Services
{
    public class HomeControllerWorkerService : IHomeControllerWorkerService
    {
        private readonly IDatabase _database;

        public HomeControllerWorkerService(IDatabase database)
        {
            _database = database;
        }


        public bool ExistsByName(string name)
        {
            if(string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
            return _database.DigitalHomes.ExitsByName(name).Any();
        }
     }
}
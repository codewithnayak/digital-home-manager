namespace DigitalHomeManager.Services
{
    public interface IHomeControllerWorkerService
    {
        bool ExistsByName(string name);
    }
}
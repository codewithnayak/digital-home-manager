using System;
using System.Threading.Tasks;
using DigitalHomeManager.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace DigitalHomeManager.Controllers.V1
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{version:apiVersion}/home")]
    public class HomeController : ControllerBase
    {

        private readonly IHomeControllerWorkerService _workerService;
        
        private readonly ILogger<HomeController> _logger;
        public HomeController(ILogger<HomeController> logger, 
            IHomeControllerWorkerService workerService)
        {
            _logger = logger;
            _workerService = workerService;
        }
        
        // GET
        [HttpGet("")]        
        public IActionResult Index()
        {
            return Ok();
        }
        
        [HttpGet("{id}")]
        public IActionResult Index(string id )
        {
            return Ok();
        }
        
        [HttpPost("create")]
        public async Task<IActionResult> Create()
        {
            var home = await Task.FromResult(new HomeDto
            {
                Id = Guid.NewGuid().ToString()
            });
            return CreatedAtAction(nameof(Index), new { id = home.Id } , "success");
        }

        [HttpGet("find/{name}")]
        public async Task<IActionResult> GetByName(string name)
        {
            var exits = await Task.FromResult(_workerService.ExistsByName(name));
            if (exits) return Ok("Already exists");
            return NotFound("Home does not exists ");
        }
    }

    public class HomeDto
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string DisplayName { get; set; }

        public AddressDto[] AddressHistory { get; set; }
    }

    public class AddressDto
    {
        public string AddressId { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2  { get; set; }

        public bool IsCurrentAddress { get; set; }

        public string PostCode { get; set; }

        public string State { get; set; }

        public string City { get; set; }

        public CountryDto Country { get; set; }
        
    }

    public class CountryDto
    {
        public string CountryId { get; set; }

        public string CountryName { get; set; }

        public string CountryCode { get; set; }
    }
}
using System;
using DigitalHome.Db;
using DigitalHomeManager.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace DigitalHomeManager
{
    public class Startup
    {
        private IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen();
            services.AddApiVersioning(config =>
            {
                config.ReportApiVersions = true;
            });
            services.AddCors();
            services.AddDbContext<DigitalHomeDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DigitalHomeDb")));
            services.AddScoped<IDatabase,Database>();
            services.AddScoped<IHomeControllerWorkerService , HomeControllerWorkerService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(config =>
            {
                //TODO:Set for specific origins 
                config.AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod();
            });

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.DocumentTitle = "Digital Home Manager";
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Address Finder V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseApiVersioning();
            
            app.UseRouting();
    
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

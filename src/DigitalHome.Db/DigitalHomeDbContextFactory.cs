using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace DigitalHome.Db
{
    public class DigitalHomeDbContextFactory : IDesignTimeDbContextFactory<DigitalHomeDbContext>
    {
        private IConfiguration Configuration =>  new ConfigurationBuilder().
            SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();
        
        public DigitalHomeDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DigitalHomeDbContext>();
            builder.UseSqlServer(Configuration.GetConnectionString("DigitalHomeDb"));
            return new DigitalHomeDbContext(builder.Options);
        }
    }
}
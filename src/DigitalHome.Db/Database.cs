using System;
using System.Linq;
using DigitalHome.Db.Entities;

namespace DigitalHome.Db
{
    public class Database : IDatabase , IDisposable
    {
        private readonly DigitalHomeDbContext _dbContext;

        public Database(DigitalHomeDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            dbContext.ChangeTracker.AutoDetectChangesEnabled = false;
        }

        /// <summary>
        /// Dispose underlying db context 
        /// </summary>
        public void Dispose() => _dbContext?.Dispose();
        
        /// <summary>
        /// The read model interface for the digital home db 
        /// </summary>
        public IQueryable<Entities.DigitalHome> DigitalHomes => _dbContext.DigitalHomes;

        /// <summary>
        /// The read model interface for address db 
        /// </summary>
        public IQueryable<Address> Addresses => _dbContext.Addresses;
    }
}
using System.Linq;
using DigitalHome.Db.Entities;

namespace DigitalHome.Db
{
    public interface IDatabase
    {
        public IQueryable<Entities.DigitalHome> DigitalHomes { get; }

        public IQueryable<Address> Addresses { get; }
    }
}
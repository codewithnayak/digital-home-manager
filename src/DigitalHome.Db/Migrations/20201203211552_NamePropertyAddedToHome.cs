﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DigitalHome.Db.Migrations
{
    public partial class NamePropertyAddedToHome : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "DigitalHomes",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "DigitalHomes");
        }
    }
}

using DigitalHome.Db.Entities;
using Microsoft.EntityFrameworkCore;

namespace DigitalHome.Db
{
    public class DigitalHomeDbContext : DbContext
    {

        public DbSet<Entities.DigitalHome> DigitalHomes { get; set; }

        public DbSet<Address> Addresses { get; set; }
        
        public DigitalHomeDbContext(DbContextOptions<DigitalHomeDbContext> options) : base(options)
        {
            Database.Migrate();
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
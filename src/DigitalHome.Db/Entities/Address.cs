using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalHome.Db.Entities
{
    public class Address 
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        
        [Required]
        public Guid AddressId { get; set; }
        
        [Required]
        [StringLength(50)]
        public string BuildingNumber { get; set; }
        
        [Required]
        [StringLength(100)]
        public string BuildingName { get; set; }
        
        [Required]
        [StringLength(100)]
        public string AddressLine1 { get; set; }

        public string AddressLine2  { get; set; }

        public bool IsCurrentAddress { get; set; } 

        [Required]
        [StringLength(100)]
        public string PostCode { get; set; }

        [Required]
        [StringLength(100)]
        public string State { get; set; }

        [Required]
        [StringLength(100)]
        public string City { get; set; }

        public DateTime LastModified { get; set; } = DateTime.UtcNow;

        //TODO:Add relationship 
        public string CountryCode { get; set; }
        
        
        //Navigation 
        [ForeignKey("DigitalHome")]
        public int DigitalHomeId { get; set; }

        public DigitalHome DigitalHome { get; set; }
    }
}
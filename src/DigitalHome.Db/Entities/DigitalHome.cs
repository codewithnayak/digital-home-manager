using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalHome.Db.Entities
{
    public class DigitalHome
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public Guid HomeId { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        
        public  ICollection<Address> Addresses { get; set; }
    }
}
using System.Linq;

namespace DigitalHome.Db.Ext
{
    public static  class DigitalHomeExtensions
    {

        public static IQueryable<Entities.DigitalHome> ExitsByName(
            this IQueryable<Entities.DigitalHome> home,
            string name)
        {
            return home.Where(_ => string.Equals(_.Name , name));
        }
    }
}
using Microsoft.Extensions.Logging;
using Moq;

namespace DigitalHomeManagerTest.Utilities
{
    public sealed class LoggerBuilder<T>
    {

        private  Mock<ILogger<T>> _logger;



        public ILogger<T> Build()
        {
            _logger = new Mock<ILogger<T>>(MockBehavior.Strict);
            
            //set up should be done here 
            
            return _logger.Object;
        }
    }
}
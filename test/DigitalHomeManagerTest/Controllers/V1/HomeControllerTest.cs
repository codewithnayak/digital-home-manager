using System.Threading.Tasks;
using DigitalHomeManager.Controllers.V1;
using DigitalHomeManager.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;
using Xunit.Abstractions;

namespace DigitalHomeManagerTest.Controllers.V1
{
    public class HomeControllerTest
    {
        private readonly Mock<ILogger<HomeController>> _loggerMock = 
            new Mock<ILogger<HomeController>>(MockBehavior.Strict);

        private readonly ITestOutputHelper _helper; 
        public HomeControllerTest(ITestOutputHelper helper)
        {
            _helper = helper;
        }

        private HomeController GetSut()
        {
            return new HomeController(_loggerMock.Object , new HomeControllerWorkerService(null));
        }

        [Fact]
        public void CanInstantiateSut()
        {
            Assert.NotNull(GetSut());
        }
        
        [Fact]
        public async Task When_create_home_called_should_return_newly_created_object_location_as_header()
        {
           
            
            var sut = GetSut();
            var response = await sut.Create();
            var actionResult = response as CreatedAtActionResult;
            
            Assert.NotNull(actionResult);
            Assert.True(actionResult.StatusCode == StatusCodes.Status201Created);
            Assert.NotNull(response);
            //make sure the action name is index and it has and id property
            Assert.Equal("Index" , actionResult.ActionName);
           
        }
        
        class ResponseType
        {
            public string id { get; set; }
        }
    }
}